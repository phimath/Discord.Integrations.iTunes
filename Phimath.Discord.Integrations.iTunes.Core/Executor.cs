﻿using System;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using iTunesLib;
using Phimath.Discord.Integrations.iTunes.Core.Discord;

namespace Phimath.Discord.Integrations.iTunes.Core
{
    public class Executor : IDisposable
    {
        private static readonly TimeSpan PollingInterval = TimeSpan.FromSeconds(1);

        private readonly CancellationTokenSource _cts;
        private readonly SemaphoreSlim _semaphore;
        private readonly object _syncRoot = new();

        private bool _isRunning = false;

        public Executor()
        {
            _semaphore = new SemaphoreSlim(1);
            _cts = new CancellationTokenSource();
        }

        public SemaphoreSlim RunBackground()
        {
            lock (_syncRoot)
            {
                if (!_isRunning)
                {
                    // Acquire before running
                    _semaphore.Wait();
                    ThreadPool.QueueUserWorkItem(Run);
                }
            }

            return _semaphore;
        }

        public void Stop()
        {
            lock (_syncRoot)
            {
                if (!_isRunning)
                {
                    return;
                }

                _cts.Cancel();
            }
        }

        public void Dispose()
        {
            _cts.Dispose();
            _semaphore.Dispose();
        }

        private async void Run(object? state)
        {
            Console.WriteLine("Running");
            try
            {
                var token = _cts.Token;

                IiTunes iTunes = new iTunesApp();
                IDiscordApp discord = new DiscordApp();

                while (!token.IsCancellationRequested)
                {
                    Console.WriteLine("Loop entry");
                    try
                    {
                        // Do polling
                        var trackIsNull = iTunes.CurrentTrack == null;
                        var stateIsPlaying = iTunes.PlayerState == ITPlayerState.ITPlayerStatePlaying;
                        if (trackIsNull)
                        {
                            discord.UnsetPresence();
                        }
                        else
                        {
                            var position = iTunes.PlayerPosition;
                            var track = iTunes.CurrentTrack!;
                            var length = track.Duration;
                            var trackName = track.Name;
                            var album = track.Album;
                            var artist = track.Artist;
                            if (stateIsPlaying)
                            {
                                discord.SetPresence(PlayerState.Playing, trackName: trackName, artist: artist,
                                    album: album, time: position, maxTime: length);
                            }
                            else
                            {
                                discord.SetPresence(PlayerState.Paused, trackName: trackName, artist: artist,
                                    album: album, time: position, maxTime: length);
                            }
                        }
                    }
                    catch (EntryPointNotFoundException)
                    {
                    }
                    catch (COMException ex)
                    {
                        await Console.Error.WriteLineAsync("Unhandled COM Exception:");
                        await Console.Error.WriteLineAsync(ex.Message);
                        await Console.Error.WriteLineAsync(ex.StackTrace);
                        discord.UnsetPresence();
                    }
                    catch (Exception ex)
                    {
                        await Console.Error.WriteLineAsync("Unhandled Exception:");
                        await Console.Error.WriteLineAsync(ex.Message);
                        await Console.Error.WriteLineAsync(ex.StackTrace);
                    }
                    finally
                    {
                        Console.WriteLine("Loop exit");
                        // "Sleep"
                        await Task.Delay(PollingInterval, token);
                    }
                }
            }
            catch (Exception ex)
            {
                await Console.Error.WriteLineAsync(ex.Message);
                await Console.Error.WriteLineAsync(ex.StackTrace);
            }
            finally
            {
                _semaphore.Release();
            }

            Console.WriteLine("Stopped");
        }
    }
}