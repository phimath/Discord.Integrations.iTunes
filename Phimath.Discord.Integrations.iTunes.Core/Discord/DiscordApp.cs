// Copyright 2021 (c) phimath.
// All rights reserved if not stated otherwise or licensed under one or more agreements.
// If applicable, license agreements can be found in the top most level of the source repository.

using System;
using System.Diagnostics.CodeAnalysis;
using Discord.Interop;

namespace Phimath.Discord.Integrations.iTunes.Core.Discord
{
    public class DiscordApp : IDiscordApp
    {
        private readonly global::Discord.Interop.Discord _backend;
        private const long ClientId = 798620322621685772L;

        public DiscordApp()
        {
            _backend = new global::Discord.Interop.Discord(ClientId, (ulong) CreateFlags.NoRequireDiscord);
        }

        public void UnsetPresence()
        {
            Console.WriteLine("UnsetPresence");
            _backend.GetActivityManager().ClearActivity(ClearActivity);
            _backend.RunCallbacks();
        }

        public void SetPresence(PlayerState state, string? trackName = null, string? artist = null,
            string? album = null, int time = -1, int maxTime = -1)
        {
            Console.WriteLine("SetPresence");

            var activity = new Activity
            {
                Type = ActivityType.Listening,
                Assets = new ActivityAssets
                {
                    LargeImage = "itunes",
                    SmallText = state.ToString(),
                    SmallImage = state.ToString().ToLowerInvariant() + "-small",
                },

                Name = "Listening",
                ApplicationId = ClientId,
            };
            
            activity.Details = string.Empty;

            if (trackName != null)
            {
                activity.Details += $"'{trackName}'";
            }

            if (artist != null)
            {
                activity.Details += $" by {artist}";
            }

            if (activity.Details == string.Empty)
            {
                activity.Details = state.ToString();
            }

            if (album != null)
            {
                activity.State = $"on {album}";
            }

            if (time != -1 && maxTime != -1)
            {
                var baseTime = DateTimeOffset.Now.ToUnixTimeSeconds();

                var startTime = baseTime - time;
                var endTime = baseTime + maxTime - time;

                activity.Timestamps = new ActivityTimestamps
                {
                    Start = startTime,
                    End = endTime
                };
            }

            _backend.GetActivityManager().UpdateActivity(activity, UpdateActivity);
            _backend.RunCallbacks();
        }

        private void UpdateActivity(Result result)
        {
            Console.WriteLine($"Update result: {result}");
        }

        private void ClearActivity(Result result)
        {
            Console.WriteLine($"Clear result: {result}");
        }

        public void Dispose()
        {
            _backend.Dispose();
        }
    }
}