// Copyright 2021 (c) phimath.
// All rights reserved if not stated otherwise or licensed under one or more agreements.
// If applicable, license agreements can be found in the top most level of the source repository.

using System;

namespace Phimath.Discord.Integrations.iTunes.Core.Discord
{
    public interface IDiscordApp : IDisposable
    {
        void UnsetPresence();

        void SetPresence(PlayerState state, string? trackName = null, string? artist = null, string? album = null,
            int time = -1, int maxTime = -1);
    }
}