﻿using System;

// ReSharper disable All
#pragma warning disable 8601
#pragma warning disable 8605
#pragma warning disable 8602
#pragma warning disable 8600
#pragma warning disable 8618

namespace Discord.Interop
{
    public partial class ActivityManager
    {
        public void RegisterCommand()
        {
            RegisterCommand(null);
        }
    }
}
