using System.Collections.Generic;

// ReSharper disable All
#pragma warning disable 8601
#pragma warning disable 8605
#pragma warning disable 8602
#pragma warning disable 8600
#pragma warning disable 8618

namespace Discord.Interop
{
    public partial class StoreManager
    {
        public IEnumerable<Entitlement> GetEntitlements()
        {
            var count = CountEntitlements();
            var entitlements = new List<Entitlement>();
            for (var i = 0; i < count; i++)
            {
                entitlements.Add(GetEntitlementAt(i));
            }
            return entitlements;
        }

        public IEnumerable<Sku> GetSkus()
        {
            var count = CountSkus();
            var skus = new List<Sku>();
            for (var i = 0; i < count; i++)
            {
                skus.Add(GetSkuAt(i));
            }
            return skus;
        }
    }
}
