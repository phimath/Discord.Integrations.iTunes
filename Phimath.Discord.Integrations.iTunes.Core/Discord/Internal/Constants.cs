﻿using System;

// ReSharper disable All
#pragma warning disable 8601
#pragma warning disable 8605
#pragma warning disable 8602
#pragma warning disable 8600
#pragma warning disable 8618

namespace Discord.Interop
{
    static class Constants
    {
        public const string DllName86 = "discord_game_sdk_x86.dll";
        public const string DllName64 = "discord_game_sdk_x64.dll";
    }
}
