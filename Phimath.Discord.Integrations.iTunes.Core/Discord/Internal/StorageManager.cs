using System;
using System.Collections.Generic;
using System.Text;

// ReSharper disable All
#pragma warning disable 8601
#pragma warning disable 8605
#pragma warning disable 8602
#pragma warning disable 8600
#pragma warning disable 8618

namespace Discord.Interop
{
    public partial class StorageManager
    {
        public IEnumerable<FileStat> Files()
        {
            var fileCount = Count();
            var files = new List<FileStat>();
            for (var i = 0; i < fileCount; i++)
            {
                files.Add(StatAt(i));
            }
            return files;
        }
    }
}
