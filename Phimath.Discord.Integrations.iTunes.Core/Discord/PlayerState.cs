// Copyright 2021 (c) phimath.
// All rights reserved if not stated otherwise or licensed under one or more agreements.
// If applicable, license agreements can be found in the top most level of the source repository.

namespace Phimath.Discord.Integrations.iTunes.Core.Discord
{
    public enum PlayerState : byte
    {
        Stopped = 0,
        Playing = 1,
        Paused = 2
    }
}