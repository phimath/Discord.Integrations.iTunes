﻿using System;
using Phimath.Discord.Integrations.iTunes.Core;

namespace Phimath.Discord.Integrations.iTunes
{
    internal static class Program
    {
        public static void Main(string[] args)
        {
            var executor = new Executor();
            executor.RunBackground().Wait();
        }
    }
}