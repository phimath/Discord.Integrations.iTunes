# Discord.Integrations.iTunes

## Usage

* Download latest release (Phimath.Discord.Integrations.iTunes.exe) from [releases page](https://gitlab.com/phimath/Discord.Integrations.iTunes/-/releases)
* Just double-click it and ...
* Your discord should now display your currently playing music from iTunes!

## Building

* Build project using Visual Studio
* Place a copy of discord-rpc.dll in the same folder as the executable. You can get a copy of this file from [here.](https://github.com/discordapp/discord-rpc/releases)

## Contact

If you would like to contact me, contact me at [nuget@phimath.de](mailto:nuget@phimath.de).
